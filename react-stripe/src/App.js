import React, {useState} from 'react';
import {loadStripe} from "@stripe/stripe-js";
import {Elements, CardElement, useStripe, useElements} from "@stripe/react-stripe-js";
import axios from 'axios';
import "bootswatch/dist/pulse/bootstrap.min.css"
import './App.css';

//Logica de Frontend


// LLave publica
// forma de conectarnos a Stripe
const stripePromise = loadStripe("pk_test_51HUjgnAk18NAkeWA8t0D6YgLuZhLOfyJ3ua9pHkhSblQXKLTs4EbW8U7FFdoaa7WDJpvCkGhEzDEyVPSj0VzWTuJ00qPmxoqnf")


//El formulario
const CheckoutForm = () => {

    const stripe = useStripe();
    const elements = useElements();

    const [loading, setLoading] = useState(false);

    const handleSubmit = async (e) => {
        e.preventDefault();

        //El llamado me devuelve un error o on Objecto en el LLamado
        const {error, paymentMethod} = await stripe.createPaymentMethod({
            type: "card",
            card: elements.getElement(CardElement),
        });
        setLoading(true);

        if (!error) {
            const {id} = paymentMethod;

            try {
                const {data} = await axios.post('http://localhost:3007/api/checkout', {
                    id,
                    amount: 1000
                })
                console.log(data)
                elements.getElement(CardElement).clear();
            } catch (e) {
                console.log(e)
            }

            setLoading(false);
        }

    };


    return (<form onSubmit={handleSubmit} className="card card-body">
            <img
                src="https://www.citypng.com/public/uploads/preview/-11594685687yk7qshpthg.png"
                alt="Boletas Netflix"
                className="img-fluid"
            />

            <h3 className="text-center my-2">Precio: $1000</h3>

            <div className="for-group">
                <CardElement className="form-control"/>
            </div>
            <button className="btn btn-success" disabled={!stripe}>
                {
                    loading ? (
                        <div className="spinner-border text-light" role="status">
                            <span className="sr-only"> Loading ...</span>

                        </div>) : (
                        "Comprar"
                    )
                }
            </button>
        </form>
    );
};


function App() {
    return (
        <Elements stripe={stripePromise}>
            <div className="container p-4">
                <div className="row">
                    <div className="col-md-4 offset-md-4">
                        <CheckoutForm/>

                    </div>
                </div>
            </div>
        </Elements>
    );
}

export default App;
